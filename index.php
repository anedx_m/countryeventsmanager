<?php

include 'config.php';

$nameController	 = isset($_GET['c']) ? $_GET['c'] : DEFAULT_CONTROLLER;
$nameController	 = $nameController . 'Controller';
$action			 = isset($_GET['act']) ? $_GET['act'] : '';

if (class_exists($nameController)) {
	$controller = new $nameController();

	if ($action) {
		$method = 'action' . $action;
	} else {
		$method = 'action' . $controller->{'defaultAction'};
	}
	if (method_exists($nameController, $method)) {
		$controller->$method();
	} else {
		echo "Action not found";
	}
} else {
	echo "Controller $nameController not found";
}

