<?php
session_start();
define('DEFAULT_CONTROLLER', 'Main');
define('APP_PATH', __DIR__);
define('BASEURL', 'http://localhost/countryeventsmanager/');

function autoloader($class) {
	$path = dirname(__FILE__) . '/classes/';
	include $path . $class . '.php';
}

spl_autoload_register('autoloader');

ORM::configure('mysql:host=192.168.1.7;dbname=countryeventsmanager');
ORM::configure('username', 'root');
ORM::configure('password', '1');
ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));