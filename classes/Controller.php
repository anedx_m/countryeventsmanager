<?php

/* Main controller class hosts common functions */

class Controller {

	const JSON_MSG_OK		 = 0;
	const JSON_MSG_ERROR	 = 1;

	public $defaultAction	 = 'default';
	public $defaultLayout	 = 'main';

	public function renderPartial($view, $param = array()) {
		if (!is_null($param)) {
			extract($param);
		}
		
		include APP_PATH . "/views/$view.php";
	}

	public function render($view, $param = array(), $layout = null) {
		if (!is_null($param)) {
			extract($param);
		}
		if (!$layout) {
			$layout = $this->defaultLayout;
		}
		include APP_PATH . "/views/layouts/{$layout}.php";
	}

	public function actionDefault() {
		echo 'default';
	}

	public function getPost($name_param) {
		return isset($_REQUEST[$name_param]) ? $_REQUEST[$name_param] : '';
	}

	public function getRequest($name_param) {
		return isset($_REQUEST[$name_param]) ? $_REQUEST[$name_param] : '';
	}

	public function getParam($name_param) {
		return isset($_GET[$name_param]) ? $_GET[$name_param] : '';
	}

	public function getValue($arr, $name_param) {
		return isset($arr[$name_param]) ? $arr[$name_param] : '';
	}

	public static function json_msg($msg, $type = self::JSON_MSG_OK) {
		echo json_encode(array('msg' => $msg, 'type' => $type));
	}

}
