<?php

class MainController extends Controller {

    public $defaultAction = 'Entry';

    public function actionEntry() {

        if (User::isGuest()) {
            header('Location: index.php?act=Authentication');
        } else {
            header('Location: index.php?act=CountryManager#Countries');
        }
    }

    public function actionCountryManager() {
        if (!User::isGuest()) {
            $this->render('main');
        }
    }

    public function actionLogout() {
        User::logout();
        header('Location: index.php?act=Authentication');
    }

    public function actionAuthentication() {
        if (User::isGuest()) {
            $this->render('authentication', null, 'login');
        } else {
            header('Location: index.php');
        }
    }

    public function actionCountries() {
        if (!User::isGuest()) {
            $this->renderPartial('countries');
        }
    }

    public function actionListCountries() {
        if (!User::isGuest()) {
            $filter = '%' . $this->getRequest('filter') . '%';
            if ($filter) {
                $countries = ORM::for_table('countries')->where_like('country', $filter)->order_by_asc('country')->find_array();
            } else {
                $countries = ORM::for_table('countries')->find_array();
            }
            $this->renderPartial('listCountries', array('countries' => $countries));
        }
    }

    public function actionLogin() {

        if (User::isGuest()) {
            $username = isset($_POST['username']) ? $_POST['username'] : '';
            $pass = isset($_POST['password']) ? $_POST['password'] : '';
            $auth = User::authenticate($username, $pass);
            if ($auth) {
                echo self::json_msg('OK');
            } else {
                echo self::json_msg('Login or Password incorrect', self::JSON_MSG_ERROR);
            }
        } else {
            echo self::json_msg('OK');
        }
    }



    public function actionUpdatePassword() {
        if (!User::isGuest()) {
            $new_pass = $this->getRequest('new_password');
//			if (!preg_match("/^[a-zA-Z0-9]+$/", $new_pass)) {
//				echo self::json_msg('Password incorrect (a-zA-Z0-9)', self::JSON_MSG_ERROR);
//				return ;
//			}
            if ($new_pass) {
                $r = User::updatePassword($new_pass);
                if ($r) {
                    echo self::json_msg('Password changed');
                } else {
                    echo self::json_msg('Error', self::JSON_MSG_ERROR);
                }
            } else {
                echo self::json_msg('The password should not be empty', self::JSON_MSG_ERROR);
            }
        }
    }

    public function actionDeleteCountry() {
        if (!User::isGuest()) {
            $country_id = $this->getRequest('country_id');
            if ($country_id) {
                Helper::deleteCountry($country_id);
                $this->json_msg('Country deleted');
            }
        }
    }

    public function actionProfile() {
        if (!User::isGuest()) {
            $this->renderPartial('profile');
        }
    }

    public function actionCreateCountry() {
        if (!User::isGuest()) {
            $country_name = $this->getRequest('country_name');
            if ($country_name) {
                $r = ORM::for_table('countries')->create();
                $r->set('country', $country_name);
                if ($r->save()) {
                    $this->json_msg('Country created');
                } else {
                    $this->json_msg('Error', self::JSON_MSG_ERROR);
                }
            }
        }
    }

    public function actionEvents() {
        $country_name = ORM::for_table("countries")
                ->where_equal("id", $this->getParam("id"))
                ->find_one();
        $cent = Helper::getCentFromBase($this->getParam("id"));
        $this->render('events', array("name" => $country_name->country, "events" => $cent));
    }

    public function actionLoadAllEvents() {
        $country_name = ORM::for_table("countries")
                ->where_equal("id", $this->getParam("id"))
                ->find_one();
        $cent = Helper::getCentFromBase($this->getParam("id"));
        $this->renderPartial("events", array("name" => $country_name->country, "events" => $cent));
    }

    public function actionEventAjax() {
        $id = $this->getParam("id");
        $mode = $this->getParam("m");

        switch ($mode) {

            case "load":
                $events = ORM::for_table("events")
                        ->where_equal("id", $id)
                        ->find_array();
                $tags = ORM::for_table("event_tags")
                        ->where_equal("event_id", $id)
                        ->find_array();
                $this->renderPartial("events_form", array("event" => $events[0], "tags" => $tags));
                break;
            case "update_country":
                $country = ORM::for_table("countries")
                        ->where_equal("id", $this->getParam("id"))
                        ->find_one();
                $country->country = $this->getParam("name");
                $country->save();
                break;
            case "new":
                $this->renderPartial("events_form_add", array("year_id" => $this->getParam("id_year")));
                break;
            case "add_year":
                $year = ORM::for_table("years")
                        ->create();
                $year->country_id = $id;
                $year->year = $this->getParam("year");
                $year->save();
                break;
            case "save":
                $tags = $this->getParam("tags");
                parse_str($this->getParam("data"), $params);
                print_r($params);
                $event = ORM::for_table("events")
                        ->where_equal("id", $params['id'])
                        ->find_one();
                $event->title = $params['title'];
                $event->title_url = $params['title_url'];
                $event->image_url = $params['image_url'];
                $event->description = $params['desc'];
                $event->date = $params['date'];
                $event->save();

                $event = ORM::for_table("event_tags")
                        ->where_equal("event_id", $params['id'])
                        ->delete_many();
                foreach ($tags as $key => $value) {
                    $tag = ORM::for_table("event_tags")
                            ->create();
                    $tag->event_id = $params['id'];
                    $tag->tag = $value;
                    $tag->save();
                }
                break;
            case "save_new":
                $tags = $this->getParam("tags");
                parse_str($this->getParam("data"), $params);
                print_r($params);
                $event = ORM::for_table("events")
                        ->create();
                $event->year_id = $params['year_id'];
                $event->title = $params['title'];
                $event->title_url = $params['title_url'];
                $event->image_url = $params['image_url'];
                $event->description = $params['desc'];
                $event->date = $params['date'];
                $event->save();
                $last_id = $event->id();

                foreach ($tags as $key => $value) {
                    $tag = ORM::for_table("event_tags")
                            ->create();
                    $tag->event_id = $last_id;
                    $tag->tag = $value;
                    $tag->save();
                }
                break;
            case "delete":
                Helper::deleteEvent($id);
                break;
            case "delete_image":
                $image = $this->getParam("image");
                Helper::deleteImage($image);

                break;
            case "delete_year":
                Helper::deleteYear($id);
                break;
            case "image_upload":
                error_reporting(0);
                define("MAX_SIZE", "9000");
                //print_r($_FILES);
                $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
                if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

                    $uploaddir = "images/"; //a directory inside
                    foreach ($_FILES['photos']['name'] as $name => $value) {

                        $filename = stripslashes($_FILES['photos']['name'][$name]);
                        $size = filesize($_FILES['photos']['tmp_name'][$name]);
                        //get the extension of the file in a lower case format
                        $str = $filename;
                        $i = strrpos($str, ".");
                        if (!$i) {
                            return "";
                        }
                        $l = strlen($str) - $i;
                        $ext = substr($str, $i + 1, $l);

                        $ext = strtolower($ext);

                        if (in_array($ext, $valid_formats)) {
                            if ($size < (MAX_SIZE * 1024)) {
                                $image_name = time() . $filename;

                                $newname = $uploaddir . $image_name;

                                if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname)) {
                                    //to base
                                    echo "<div class='show-image'>";
                                    echo "<img src='" . $uploaddir . $image_name . "' class='imgList'>";
                                    echo '<input class="delete btn btn-danger" type="button" value="Delete" onclick="$(this).parent().remove();deleteImage(\'' . $uploaddir . $image_name . '\')"/>';
                                    echo "</div><script>$('#image_url').val('" . $uploaddir . $image_name . "')</script>";
                                } else {
                                    echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                                }
                            } else {
                                echo '<span class="imgList">You have exceeded the size limit!</span>';
                            }
                        } else {
                            echo '<span class="imgList">Unknown extension!</span>';
                        }
                    }
                }


                break;
        }
    }

    public function actionGetHTML() {
        $country_id = $this->getRequest('id');
        if (!$country_id)
            die();
        $country = ORM::for_table('countries')->where('id', $country_id)->find_one();
        $years = ORM::for_table('years')->raw_query("SELECT * FROM years WHERE country_id=$country_id ORDER BY year DESC")->find_array();
        $events = array();
        $events['country'] = $country->country;
        $events['years'] = array();
        foreach ($years as $year) {
            $events['years'][$year['year']] = ORM::for_table('events')->raw_query("SELECT * FROM events WHERE {$year['id']} = year_id ORDER BY IF(date='0000-00-00', 0, 1) ASC, date DESC")->find_array();
        }
        $this->renderPartial('html', array('events' => $events));
    }

}
