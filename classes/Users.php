<?php

/* user managment functionality */

class User {

	public $id;
	public $usename;
	private static $instance;

	/**
	 * 
	 * @return User
	 */
	public static function getInstance() {
//        var_dump(self::$instance);
		if (self::$instance) {
			return self::$instance;
		} else {
			self::$instance = new User();
			return self::$instance;
		}
	}

	public static function authenticate($username, $pass) {

		if (!preg_match("/^[a-zA-Z0-9]+$/", $username)) {
			return FALSE;
		}
//		if (!preg_match("/^[a-zA-Z0-9]+$/", $pass)) {
//			return FALSE;
//		}
		$tUser = self::getUser($username, $pass);

		if ($tUser) {
			$_SESSION['userid'] = $tUser['id'];
			return TRUE;
		}
		return FALSE;
	}

	public static function isGuest() {
		if (isset($_SESSION['userid'])) {
			return false;
		} else {
			return true;
		}
	}

	public static function logout() {
		unset($_SESSION['userid']);
		self::$instance = null;
	}

	private static function getUser($username, $pass) {
		$pass	 = self::encript($pass);
		$user	 = ORM::for_table('users')
		->where('username', $username)
		->where('password', $pass)
		->find_one();
		return $user;
	}

	/**
	 * 
	 * @param type $id_user
	 * @return ORM
	 */
	public static function getUserByID($id_user) {
		$user = ORM::for_table('users')
		->where('id', $id_user)
		->find_one();
		return $user;
	}

	public function loadUser($id_user = null) {
		if (!$id_user) {
			if (isset($_SESSION['userid'])) {
				$id_user = $_SESSION['userid'];
			} else {
				return 0;
			}
		}
		$tUser = self::getUserByID($id_user);
		if ($tUser) {
			$user = $tUser->as_array();
			foreach ($user as $field => $value) {
				if($field=='password')
					continue;
				$this->{$field} = $value;
			}
		}
		return $this->id;
	}

	public static function updatePassword($new_pass) {
		$user = self::getInstance();
		$tUser			 = self::getUserByID($user->id);
		$tUser->password = self::encript($new_pass);
		return $tUser->save();
	}
	
	public static function updateUploadLimit($new_limit) {
		$user = self::getInstance();
		$tUser			 = self::getUserByID($user->id);
		$tUser->upload_limit = $new_limit;
		return $tUser->save();
	}

	private static function encript($pass) {
		return md5($pass);
	}
}