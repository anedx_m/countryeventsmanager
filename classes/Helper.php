<?php

class Helper {

    public static function Hello() {
        echo 'Hello';
    }

    public static function getCentFromBase($id) {
        $years = ORM::for_table("years")
                ->select("id")
                ->select("year")
                ->where_equal("country_id", $id)
                ->order_by_desc('year')
                ->find_array();

        $cent = array();
        foreach ($years as $key => $year) {
            $events = ORM::for_table("events")
                    ->where_equal("year_id", $year['id'])
                    ->order_by_expr("IF(date='0000-00-00',0,1) ASC")
                    ->order_by_desc("date")
                    ->find_array();
            foreach($events as $k=>$e) 
				$events[$k]['tags'] = join(",", Helper::getEventTags($e['id']) );
            $years[$key]['events'] = $events;
            $cent[floor($year['year'] / 100) + 1][] = $years[$key];
            unset($years[$key]);
        }
        return $cent;
    }
    
    public static function getEventTags($id) {
		$records = ORM::for_table("event_tags")
                        ->where_equal("event_id", $id)
                        ->find_array();
        $tags = array();                
        foreach($records as $r)                
			$tags[] = $r['tag'];
        return $tags;                
	}
	
    public static function deleteEvent($id) {
        $event = ORM::for_table("events")
                ->where_equal("id", $id)
                ->find_one();
        $tags = ORM::for_table("event_tags")
                ->where_equal("event_id", $id)
                ->delete_many();      
        Helper::deleteImage($event->image_url);
        $event->delete();
    }

    public static function deleteYear($id) {

        $year = ORM::for_table("years")
                ->where_equal("id", $id)
                ->delete_many();

        $event = ORM::for_table("events")
                ->where_equal("year_id", $id)
                ->find_array();

        foreach ($event as $value) {
            Helper::deleteEvent($value['id']);
            
        }
    }

    public static function deleteCountry($id) {

        $country = ORM::for_table("countries")
                ->where_equal("id", $id)
                ->delete_many();

        $years = ORM::for_table("years")
                ->where_equal("country_id", $id)
                ->find_array();
        foreach ($years as $value) {
            Helper::deleteYear($value['id']);            
        }
    }

    public static function deleteImage($image) {
        unlink(dirname(__FILE__) . "/../" . $image);
    }
    
    
    
}
