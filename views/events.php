<div class="container">

    <div class="row">
        <div class="col-lg-2 form-group">
            <label for="country">Country name:</label>
            <input class="form-control" type='text' id="country" value="<?= $name ?>"/>
            <button type="button" onclick="update_country();" class="btn btn-success">Update Country</button>
        </div>

        <div class="col-lg-2 form-group"><label for="year">Year:</label>
            <input type='text' class="form-control" id="year" value=""/>
            <button type="button" onclick="add_year();" class="btn btn-success">Add Year</button>
        </div>

    </div>

    <br>
    <div class="row">
        <div class="panel-group" id="accordion1"> 

            <?php foreach ($events as $cent_k => $cent): ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseCent<?= $cent_k ?>">
                                <?= $cent_k ?> Century
                            </a></h4>
                    </div>
                    <div id="collapseCent<?= $cent_k ?>" class="panel-collapse collapse">
                        <div class="panel-body">

                            <!-- Here we insert another nested accordion -->
                            <div class="panel-group" id="accordionYear<?= $cent_k ?>">
                                <?php foreach ($cent as $year): ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionYear<?= $cent_k ?>" href="#collapseYearaccordionYear<?= $year['year'] ?>">
                                                    <?= $year['year'] ?>
                                                </a><div class="pull-right"><button onclick="add_event(<?=$year['id']?>);" type="button" class="btn-xs btn-success ">Add Event</button><button onclick="delete_year(<?=$year['id']?>);" type="button" class="btn-xs btn-danger ">Delete Year</button></div></h4>
                                        </div>
                                        <div id="collapseYearaccordionYear<?= $year['year'] ?>" class="panel-collapse collapse">
                                            <div class="panel-body">                                              
                                                <!--                                                Загрузка эвентов-->
                                                <?php foreach ($year['events'] as $event_k => $event): ?> 
                                                    <div class="jumbotron">
                                                        <dt><?= ($event['date']=='0000-00-00') ? "date N/A" : date("j M",strtotime($event['date'])) ?>
                                                        <?php if($event['title_url']): ?>
															<a href="<?= $event['title_url'] ?>" target=_blank>More Info</a>
														<?php endif; ?>	
                                                        <?php if($event['image_url']): ?>
															<a href="<?= $event['image_url'] ?>" target=_blank>View Image</a>
														<?php endif; ?>	
                                                        <big><span onclick="edit_event(<?=$event['id']?>);" class="glyphicon glyphicon-pencil text-success" aria-hidden="true"></span></big>
                                                       <big><span onclick="delete_event(<?=$event['id']?>);" class="pull-right glyphicon glyphicon-trash text-danger" aria-hidden="true"></span></big>    </dt>                                                 
                                                        <dt><?= $event['title'] ?></dt>
                                                        <dd><?= $event['description'] ?></dd>                                                      
                                                        <?php if($event['tags']): ?>
															<dd> <b>Tags:</b>  <?= $event['tags'] ?></dd>
														<?php endif; ?>	
                                                    </div>
                                                <?php endforeach; ?> 
                                            </div>
                                        </div>
                                    </div>

                                <?php endforeach; ?> 
                            </div>
                            <!-- Inner accordion ends here -->


                        </div>
                    </div>
                </div>

            <?php endforeach; ?> 

        </div> 


    </div>
</div>




<div id="defaultModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="modal-label">Edit Event</h4>
            </div>
            <div id="eventModal" class="modal-body">                   
            </div>
        </div> <!--modal-content-->
    </div> <!--modal-dialog-->
</div> <!--modal-->


