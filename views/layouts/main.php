<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Events Manager</title>
        <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/datepicker3.css">
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/js.js"></script>
        <script type="text/javascript" src="assets/js/jquery.wallform.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap-datepicker.js"></script>
        
		<!-- include summernote -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/summernote.css">
		<script type="text/javascript" src="assets/js/summernote.min.js"></script>
  
    </head>    
    <body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Country events manager</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li ><a data-hash="Countries"  href="index.php?act=Countries">Countries</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li ><a data-hash="Profile" href="index.php?act=Profile">Profile</a></li>
						<li class="nav-Logout"><a href="index.php?act=Logout"><?php
								$user = User::getInstance();
								if ($user->isGuest()) {
									
								} else {
									echo $user->username . " (logout)";
								}
								?></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->

			</div><!-- /.container-fluid -->
		</nav>
		<div class="content">
			<?php
			$this->renderPartial($view, $param);
			?>
		</div>
		<script>
			$( document ).ready( function() {
				var active_tab = document.location.hash.substr( 1 );
				if ( active_tab == '' )
					return;
				var a = $( '#bs-example-navbar-collapse-1 a[data-hash=' + active_tab + ']' )
				a.closest( 'li' ).addClass( 'active' )
				a.click()
			} )

			$( '#bs-example-navbar-collapse-1 li:not(.nav-Logout) a' ).click( function() {
				$( '#bs-example-navbar-collapse-1 li' ).removeClass( 'active' )
				$( this ).closest( 'li' ).addClass( 'active' );
				var hash = $( this ).attr( 'data-hash' );
				document.location.hash = hash;
				var url = getJsonFromUrl();
				if ( url['act'] !== 'CountryManager' ) {
					document.location.search = 'index.php?act=CountryManager';
				}
				var url = $( this ).attr( 'href' );
				$.post( url, '', function( data ) {
					$( '.content' ).html( data )
				} )
				return false;
			} )
		</script>
    </body>
</html>



