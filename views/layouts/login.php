<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
        <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    </head>   
	<body>
		<?php
		$this->renderPartial($view, $param);
		?>
	</body>
</html>