<div class="container">
	<div class="login-vertical-align">

		<div class="login-container col-centered ">
			<div class="login-header">Country events manager</div>
			<div class="info-login"></div>

			<form class="form-login" action="" method="">
				<div class="form-group">
					<input name="username" class="form-control" type="text" placeholder="username">
					<input type="password" name="password"  class="form-control" placeholder="password">
					<button class="btn btn-info btn-block button-login" type="submit">Login</button>
				</div>
			</form>

		</div>
	</div>
</div>


<script>

	$( '.button-login' ).click( function() {
//        alert(1);
		$.post( 'index.php?act=Login', $( '.form-login' ).serialize(), function( r ) {
//            alert(r.type!=MESSAGE_TYPE_SUCCESS);
			if ( r.type != 0 ) {
				$( '.info-login' ).html( r.msg )
			} else {
				document.location = 'index.php';
			}
		}, 'JSON' )
		return false;
	} )

</script>