<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>assets/css/countryevents.css">
<div class="history <?php echo $events['country'] ?>">
	<?php
	foreach ($events['years'] as $year => $events_item) {
//	var_dump($events_item);
		?>
		<div class="year-item">
			<div class="year"><?php echo $year ?></div>
			<?php
			foreach ($events_item as $item) {
				?>
				<div class="item">
					<?php if($item['date']!='0000-00-00'): ?>
                        <div class="date"><?php echo date("j M", strtotime($item['date'])) ?></div>
					<?php endif; ?>
					<?php if($item['title']): ?>
						<div class=”title”><?php echo $item['title'] ?></div>
					<?php endif; ?>
					<?php if($item['description']): ?>
						<div class="description"><?php echo $item['description'] ?></div>
					<?php endif; ?>
					<?php if($item['title_url']): ?>
						<a href="<?php echo $item['title_url'] ?>" target=_blank>More Info</a>
					<?php endif; ?>

					<?php if($item['image_url']): ?>
						<img src="<?php echo BASEURL.$item['image_url'] ?>" />
					<?php endif; ?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	}
	?>
</div>