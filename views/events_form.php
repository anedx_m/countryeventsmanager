<form id="form_event">
     <input type="hidden" class="form-control" name="id" id="event_id" value="<?= $event['id'] ?>">
     <div class="form-group">
         <label for="date">Date:</label>
         <input type="text" class="form-control" name="date" id="date" value="<?= ($event['date']=='0000-00-00') ? "" : $event['date']?>" placeholder="Click for date input">
     </div>
    <div class="form-group">
        <label for="titleEv">Title:</label>
        <input type="text" class="form-control" maxlength="30"  name="title" id="titleEv" value="<?= $event['title'] ?>" placeholder="Enter title">
    </div>
    <div class="form-group">
        <label for="urlEv">More info URL:</label>
        <input type="text" class="form-control" name="title_url" id="title_url" value="<?= $event['title_url'] ?>" placeholder="Enter url">
    </div>
    <div class="form-group">
        <label for="descEv">Description:</label>
        <textarea id="desc" name="desc" rows="5" class="form-control"><?= $event['description'] ?></textarea>
    </div>
     <input type="hidden" class="form-control" name="image_url" id="image_url" value="<?= $event['image_url'] ?>">
                                                        

</form>
<div>
   <div style='padding-left: 0px;' class=" form-group col-lg-2"> <input type="text" class="form-control" id="add_tags">
  <button type="button" onclick="add_tag()" class="btn-xs btn-primary">add tag</button></div>
    <div class="col-lg-6" id="div_tags">
         <?php foreach ($tags as $key => $tag): ?>
        <div class='tag'><?=$tag['tag']?> <a style='' href='' onclick='$(this).parent().remove();return false;'>x</a></div>
        <?php endforeach; ?> 
    </div>
  <br>
</div>
<br>
<br>
<br>
<div id='preview' class="preview">
            </div>
<form id="imageform" method="post" enctype="multipart/form-data" action='index.php?act=eventAjax&m=image_upload' style="clear:both">
    <div class="form-group">
    <h4>Upload your images</h4> 
    <div id='imageloadstatus' style='display:none'><img src="assets/images/loader.gif" alt="Uploading...."/></div>
    <div id='imageloadbutton'>
        <input type="file" name="photos[]" id="photoimg" multiple="false" />
    </div>
    </div>
</form>
<button type="button" onclick="save_event(<?= $event['id'] ?>)" class="btn btn-success">Save</button>

  <script type="text/javascript">
    $(function() {
      $('#desc').summernote({
        height: 200
      });
    });
  </script>
