<nav class="navbar navbar-default navbar navbar-left ">
	<div class="container">
		<div class="navbar-form">
			<label for="filter-country">Filter: </label>
			<input name="filter-country" class="countries-filter form-control" placeholder="enter country">
			<!--<span class="splitter" style="padding: 3px; color: #b9b9b9">|</span>-->
			<a class="btn btn-default pull-right" id="add_country">Add Country</a>
		</div>
	</div>
</nav>
<div class="clearfix"></div>

<div class="list-countries"></div>
<div class="country-dialog hide">
	<form class="country-d" onsubmit="return false;">
		<input name="country_name" placeholder="Country name">
	</form>
</div>
<script>
	$( document ).ready( function() {
		listCountries()
		$( '.countries-filter' ).keyup( function( e ) {
			listCountries()
		} )
	} )
	function listCountries() {
		var filter = $( '.countries-filter' ).val();
		$.post( 'index.php?act=ListCountries', 'filter=' + filter, function( data ) {
			$( '.list-countries' ).html( data )
			$( '.country-delete' ).click( function() {
				var that = this
				bootbox.confirm( 'Are you sure?', function( result ) {
					var country_id = $( this ).attr( 'data-country_id' );
					if ( result ) {
						var country_id = $( that ).attr( 'data-country_id' )
						$.post( 'index.php?act=DeleteCountry', 'country_id=' + country_id, function( data ) {
							$( that ).closest( '.country' ).remove();
						}, 'JSON' )
					}
				} )




			} )
			$( '.country-edit' ).click( function() {
				var country_id = $( this ).attr( 'data-country_id' );
				document.location = 'index.php?act=Events&id=' + country_id;
				return false;
			} )
		} )
	}

	$( '#add_country' ).click( function( ) {
		var d = $( '.country-dialog' ).clone();
		d.find( '.country-d' ).removeClass().addClass( 'country-d-b' )
		bootbox.dialog( {
			message: d.html(),
			title: "Add country",
			width: 100,
			buttons: {
				Cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function( ) {
						this.hide()
					}
				},
				Ok: {
					label: "Ok",
					className: "btn-primary",
					callback: function( ) {
						console.log( $( '.country-d-b' ).serialize() )
						$.post( 'index.php?act=CreateCountry', $( '.country-d-b' ).serialize(), function( data ) {
							bootbox.alert( data.msg )
							listCountries();
						}, 'JSON' );
						this.hide()
					}
				},
			}
		} )
	} )
</script>
