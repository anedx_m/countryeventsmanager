<div class="border-single col-lg-2 col-centered">
	<form class="form-new-password">
		<div class="form-group">
			<label for="new_password">Change password</label>
			<input name="new_password" class="form-control" placeholder="New password">
			<input class="btn btn-default btn-update margin-10-top" type="button" value="Update">
		</div>
	</form>
	<div class="clearfix"></div>
</div>

<script>
	$( '.btn-update' ).click( function() {
		$.post( 'index.php?act=UpdatePassword', $( '.form-new-password' ).serialize(), function( data ) {
			bootbox.alert( data.msg )
		}, 'JSON' )
	} )
</script>
