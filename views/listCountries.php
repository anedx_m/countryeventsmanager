<?php
foreach ($countries as $country) {
	?>
	<div class="country bg-info">
		<div><img class="country-delete" data-country_id="<?php echo $country['id'] ?>" src="assets/img/delete.png"></div>
		<div class="country-id"><span style="font-weight: bold">id:</span> <?php echo $country['id'] ?></div>
		<div class="country-name" ><span style="font-weight: bold">country:</span> <a class="country-edit" href="" data-country_id="<?php echo $country['id'] ?>"><?php echo $country['country'] ?></a></div>
	</div>
	<?php
}
?>
