/* 
 * Tatarchuk E.N.
 * 
 * 
 */

$(function() {


    $('#defaultModal').modal({show: false});

    $('#defaultModal').on('hidden.bs.modal', function() {
        //deleteImage($("#image_url").val());
    })

});

function edit_event(id)
{
    //$('#eventModal').html(id);

    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            id: id,
            m: "load",
        },
        success: function(data) {

            $('#eventModal').html(data);
            jQuery('#photoimg').off("click").on('change', function() {
                //jQuery("#preview").html('');

                jQuery("#imageform").ajaxForm({target: '#preview',
                    beforeSubmit: function() {
                        jQuery("#imageloadstatus").show();
                        jQuery("#imageloadbutton").hide();
                    },
                    success: function() {
                        jQuery("#imageloadstatus").hide();
                        jQuery("#imageloadbutton").show();
                        jQuery("#imageloadbutton").hide();
                    },
                    error: function() {
                        jQuery("#imageloadstatus").hide();
                        jQuery("#imageloadbutton").show();
                    }}).submit();


            });
            if (($("#image_url").val() !== undefined) && ($("#image_url").val() !== "")) {
                var html = "";
                jQuery("#imageloadbutton").hide();
                html += "<div class='show-image'>";
                html += "<img src='" + $("#image_url").val() + "' class='imgList'>";
                html += '<input class="delete btn btn-danger" type="button" value="Delete" onclick="$(this).parent().remove();deleteImage(\'' + $("#image_url").val() + '\')"/>';
                html += "</div>";
                $("#preview").html(html)
            }
            else
            {
                jQuery("#imageloadbutton").show();

            }

            $('#date').datepicker({
                format: "yyyy-mm-dd"
            });
            $('#defaultModal').modal('show');
        },
    });
}

function add_event(id)
{
    //$('#eventModal').html(id);

    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            id_year: id,
            m: "new",
        },
        success: function(data) {

            $('#eventModal').html(data);
            jQuery('#photoimg').off("click").on('change', function() {
                //jQuery("#preview").html('');

                jQuery("#imageform").ajaxForm({target: '#preview',
                    beforeSubmit: function() {
                        jQuery("#imageloadstatus").show();
                        jQuery("#imageloadbutton").hide();
                    },
                    success: function() {
                        jQuery("#imageloadstatus").hide();
                        jQuery("#imageloadbutton").show();
                        jQuery("#imageloadbutton").hide();
                    },
                    error: function() {
                        jQuery("#imageloadstatus").hide();
                        jQuery("#imageloadbutton").show();
                    }}).submit();


            });
            if (($("#image_url").val() !== undefined) && ($("#image_url").val() !== "")) {
                var html = "";
                jQuery("#imageloadbutton").hide();
                html += "<div class='show-image'>";
                html += "<img src='" + $("#image_url").val() + "' class='imgList'>";
                html += '<input class="delete btn btn-danger" type="button" value="Delete" onclick="$(this).parent().remove();deleteImage(\'' + $("#image_url").val() + '\')"/>';
                html += "</div>";
                $("#preview").html(html)
            }
            else
            {
                jQuery("#imageloadbutton").show();

            }
            $('#date').datepicker({
                format: "yyyy-mm-dd"
            });
            $('#defaultModal').modal('show');
        },
    });
}

function save_event(id)
{ 
    var errors=[]
    if ($('#title_url').val()!=="")
		if (!validateURL($('#title_url').val())){
			errors.push("Wrong URL format!")
		}
		
    if (($('#desc').val()==="")){
			errors.push("Empty Description!")
	}
    //console.log(errors);
    
    if (errors.length==0){
		
    $('textarea[name="desc"]').html($('#desc').code());// wisiwing
    var data = $('form#form_event').serialize();
	//alert(data)
    //$('#eventModal').html(id);
    $('#defaultModal').modal('show');
    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            id: id,
            data: data,
            tags: parse_tags(),
            m: "save",
        },
        success: function(data) {
            loadAllEvents();

        },
    });
    }else {
 bootbox.alert(errors.join("<br>"), function() {
  
});
    }
    
}

function delete_event(id)
{
    bootbox.confirm("Are you sure?", function(result) {

        if (result) {
            $.ajax({
                url: "index.php?act=eventAjax",
                dataType: "html",
                data: {
                    id: id,
                    m: "delete",
                },
                success: function(data) {
                    loadAllEvents();
                },
            });
        }
    });
}

function delete_year(id)
{
    bootbox.confirm("Are you sure?", function(result) {

        if (result) {
            $.ajax({
                url: "index.php?act=eventAjax",
                dataType: "html",
                data: {
                    id: id,
                    m: "delete_year",
                },
                success: function(data) {
                    loadAllEvents();
                },
            });
        }
    });
}

function save_event_new()
{
    var data = $('form#form_event').serialize();
    //$('#eventModal').html(id);
    $('#defaultModal').modal('show');
    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            data: data,
            tags: parse_tags(),
            m: "save_new",
        },
        success: function(data) {
            loadAllEvents();

        },
    });
}

function deleteImage(image)
{

    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            image: image,
            m: "delete_image",
        },
        success: function(data) {
            $("#image_url").val("");
            jQuery("#imageloadbutton").show();

        },
    });

}

function loadAllEvents()
{
    var state = $(".in");

    var p = getJsonFromUrl();
    $.ajax({
        url: "index.php?act=loadAllEvents",
        dataType: "html",
        data: {
            id: p.id,
        },
        success: function(data) {

            $(".content").html(data);
            $.each(state, function(key, value) {
                console.log(value.id);
                $("#" + value.id + "").addClass("in");
            });


            $('#defaultModal').on('hidden.bs.modal', function() {
              // deleteImage($("#image_url").val());
            })


        },
    });
}

function getJsonFromUrl() {
    var query = location.search.substr(1);
    var result = {};
    query.split("&").forEach(function(part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}

function update_country()
{
    var p = getJsonFromUrl();
    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            id: p.id,
            name: $("#country").val(),
            m: "update_country"
        },
        success: function(data) {
            loadAllEvents();
        },
    });
}

function add_year()
{
    var p = getJsonFromUrl();
    $.ajax({
        url: "index.php?act=eventAjax",
        dataType: "html",
        data: {
            id: p.id,
            year: $("#year").val(),
            m: "add_year"
        },
        success: function(data) {
            loadAllEvents();
        },
    });
}

function add_tag()
{
    var $tag = $("#add_tags").val();
    var html = "<div class='tag'>" + $tag + " <a style='' href='' onclick='$(this).parent().remove();return false;'>x</a></div>";
    $("#div_tags").append(html);
    $("#add_tags").val("");
}

function parse_tags()
{
    var tags = $(".tag");
    var t_arr = [];
    $.each(tags, function(key, value) {
        t_arr.push($(value).text().substring(0, $(value).text().length - 2));
    });
    return t_arr;


}

function validateURL(textval) {
      var urlregex = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
      return urlregex.test(textval);
    }

